package com.example.natnisha.eventy.retrofit;

import com.example.natnisha.eventy.DOM.DetailResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitInterface {
    @GET("ikku/api/activities/services/topActivity.php")
    Call<DetailResponse> getEventList();
}
