package com.example.natnisha.eventy.holder;

import com.example.natnisha.eventy.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by natnisha on 22/2/2018 AD.
 */

public class EventDetailHolder extends RecyclerView.ViewHolder {
    public TextView tv_date, tv_month, tv_event_name, tv_event_time, tv_event_place;
    public ImageView event_image;


    public EventDetailHolder(View itemView) {
        super(itemView);
        event_image = itemView.findViewById(R.id.event_image);
        tv_event_name = itemView.findViewById(R.id.tv_event_name);
    }

}
