package com.example.natnisha.eventy.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.natnisha.eventy.R;


public class SectionViewHolder extends RecyclerView.ViewHolder {
    public TextView tvSection;

    public SectionViewHolder(View itemView) {
        super(itemView);
        tvSection = (TextView) itemView.findViewById(R.id.tv_section);
    }
}