package com.example.natnisha.eventy;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class EventInfoActivity extends AppCompatActivity implements OnMapReadyCallback{

    TextView tv_event_type,tv_event_name,tv_event_time,tv_interested,tv_event_info;
    Typeface tf_helvetica, AXIS;


    GoogleMap mGoogleMap;
    MapView mMapView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_info);

        tf_helvetica = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
        AXIS = Typeface.createFromAsset(getAssets(), "Axis Extrabold.otf");

        tv_event_type = findViewById(R.id.tv_event_type);
        tv_event_name = findViewById(R.id.tv_event_name);
        tv_event_time = findViewById(R.id.tv_event_time);
        tv_interested = findViewById(R.id.tv_interested);
        tv_event_info = findViewById(R.id.tv_event_info);

        tv_event_type.setTypeface(AXIS);
        tv_event_info.setTypeface(tf_helvetica);
        tv_event_name.setTypeface(tf_helvetica);
        tv_event_time.setTypeface(tf_helvetica);
        tv_interested.setTypeface(tf_helvetica);

        tv_event_type.setText(R.string.movie);
        tv_event_name.setText("hemlock grove");
        tv_event_time.setText("Today 3.00 pm.");
        tv_interested.setText("60 interested");
        tv_event_info.setText(R.string.string_event_info);

        mMapView = (MapView) findViewById(R.id.map);
        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getApplicationContext());
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng position = new LatLng(16, 102);
        mGoogleMap.addMarker(new MarkerOptions().position(position));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(position));

    }
}
