package com.example.natnisha.eventy;


import android.app.FragmentTransaction;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.natnisha.eventy.Fragment_bottom_bar.AccountFragment;
import com.example.natnisha.eventy.Fragment_bottom_bar.CalendarFragment;
import com.example.natnisha.eventy.Fragment_bottom_bar.HomeFragment;
import com.example.natnisha.eventy.recycle_view.EventListAdapter;


public class HomeActivity extends AppCompatActivity {
    BottomNavigationView navigation;
    HomeFragment fragmentHome;
    FragmentManager managerHome,managerCalendar,managerAccount;
    CalendarFragment fragmentCalendar;
    AccountFragment fragmentAccount;

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    android.support.v4.app.FragmentTransaction fragmentTransactionHome = managerHome.beginTransaction();
                    fragmentTransactionHome.replace(R.id.frame, fragmentHome);
                    fragmentTransactionHome.commit();
                    return true;
                case R.id.navigation_calendar:
                    android.support.v4.app.FragmentTransaction fragmentTransactionCalendar = managerCalendar.beginTransaction();
                    fragmentTransactionCalendar.replace(R.id.frame,fragmentCalendar);
                    fragmentTransactionCalendar.commit();
                    return true;
                case R.id.navigation_account:

                    android.support.v4.app.FragmentTransaction fragmentTransactionAccount = managerAccount.beginTransaction();
                    fragmentTransactionAccount.replace(R.id.frame,fragmentAccount);
                    fragmentTransactionAccount.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //define home fragment
        fragmentHome = new HomeFragment();
        managerHome = getSupportFragmentManager();
        //define calendar fragment
        fragmentCalendar = new CalendarFragment();
        managerCalendar = getSupportFragmentManager();
        //define account fragment
        fragmentAccount= new AccountFragment();
        managerAccount = getSupportFragmentManager();

        //call home fragment
        android.support.v4.app.FragmentTransaction fragmentTransactionHome = managerHome.beginTransaction();
        fragmentTransactionHome.replace(R.id.frame, fragmentHome);
        fragmentTransactionHome.commit();

        //check the fragment that user's choosing
        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);



    }

}
