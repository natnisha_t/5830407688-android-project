package com.example.natnisha.eventy.holder;

/**
 * Created by natnisha on 22/2/2018 AD.
 */
import android.support.v7.widget.RecyclerView;
import android.view.View;


public class EmptyViewHolder extends RecyclerView.ViewHolder {
    public EmptyViewHolder(View itemView) {
        super(itemView);
    }
}