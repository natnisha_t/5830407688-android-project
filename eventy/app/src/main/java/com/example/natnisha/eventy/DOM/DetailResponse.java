package com.example.natnisha.eventy.DOM;

import java.util.List;

public class DetailResponse {

    private List<ActivitiesBean> activities;

    public List<ActivitiesBean> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivitiesBean> activities) {
        this.activities = activities;
    }

    public static class ActivitiesBean {
        /**
         * title : เสิ่นสงกรานต์ อีสานบ้านเฮา 2018
         * dateSt : 2018-04-10
         * timeSt : AM. 16.30
         * dateEd : 2018-04-10
         * timeEd : AM. 21.00
         * duration : -
         * audience :
         * content : คณะบริหารธุรกิจและการบัญชี โดยนักศึกษาสาขาการจัดการการท่องเที่ยว จัดกิจกรรม  &amp;quot;เสิ่นสงกรานต์ อีสานบ้านเฮา 2018&amp;quot;  ภายใต้รายวิชา 964 238 Event Management การจัดการงานเทศกาล  โดยมี ผศ.ดร. ดลฤทัย โกวรรธนะกุล อาจารย์ประจำรายวิชาทั้งงานจะจัดขึ้นในวันที่ 10 เมษายน 2561 เวลา 16:30 - 21:00 น. ณ ตลาด 62 Blocs (กังสดาล) ภายในงานมีกิจกรรมมากมายไม่ว่าจะเป็น-การทำworkshop-ร้านอาหารอีสาน-บูธให้ความรู้เกี่ยวกับอุตสาหกรรมการท่องเที่ยวต่างๆ-การประกวดนางเสิ่นสงกรานต์2018-Mini concert จากวง &amp;quot;อ้ายมีผัวแล้ว&amp;quot;รายละเอียดเพิ่มเติมที่แฟนเพจ &amp;quot;เสิ่น&amp;quot;  (https://www.facebook.com/%E0%B9%80%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%99-1666781736743470)
         * image : http://www.kku.ac.th/ikku/api/images/data/5caY2YZwMnlwyideI2HFrr6KYImr.png
         * sponsor : คณะบริหารธุรกิจและการบัญชี
         * place : ตลาด 62 Blocs (กังสดาล)
         * contact : {"phone":"043 202 401","email":"","website":"https://www.facebook.com/%E0%B9%80%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%99-1666781736743470"}
         * test_img : I0133513
         * url : http://www.kku.ac.th/events/v.php?q=0000996&l=th
         */

        private String title;
        private String dateSt;
        private String timeSt;
        private String dateEd;
        private String timeEd;
        private String duration;
        private String audience;
        private String content;
        private String image;
        private String sponsor;
        private String place;
        private ContactBean contact;
        private String test_img;
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDateSt() {
            return dateSt;
        }

        public void setDateSt(String dateSt) {
            this.dateSt = dateSt;
        }

        public String getTimeSt() {
            return timeSt;
        }

        public void setTimeSt(String timeSt) {
            this.timeSt = timeSt;
        }

        public String getDateEd() {
            return dateEd;
        }

        public void setDateEd(String dateEd) {
            this.dateEd = dateEd;
        }

        public String getTimeEd() {
            return timeEd;
        }

        public void setTimeEd(String timeEd) {
            this.timeEd = timeEd;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getAudience() {
            return audience;
        }

        public void setAudience(String audience) {
            this.audience = audience;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSponsor() {
            return sponsor;
        }

        public void setSponsor(String sponsor) {
            this.sponsor = sponsor;
        }

        public String getPlace() {
            return place;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public ContactBean getContact() {
            return contact;
        }

        public void setContact(ContactBean contact) {
            this.contact = contact;
        }

        public String getTest_img() {
            return test_img;
        }

        public void setTest_img(String test_img) {
            this.test_img = test_img;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public static class ContactBean {
            /**
             * phone : 043 202 401
             * email :
             * website : https://www.facebook.com/%E0%B9%80%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%99-1666781736743470
             */

            private String phone;
            private String email;
            private String website;

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getWebsite() {
                return website;
            }

            public void setWebsite(String website) {
                this.website = website;
            }
        }
    }
}
