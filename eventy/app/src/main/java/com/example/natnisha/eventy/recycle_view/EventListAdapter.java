package com.example.natnisha.eventy.recycle_view;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.natnisha.eventy.DOM.DetailResponse;
import com.example.natnisha.eventy.EventInfoActivity;
import com.example.natnisha.eventy.R;
import com.example.natnisha.eventy.holder.EventDetailHolder;
import static java.lang.String.valueOf;

/**
 * Created by natnisha on 22/2/2018 AD.
 */

public class EventListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private DetailResponse detailResponse;
    private Context myFuckingContext;

    public void setData(DetailResponse detailResponse) {
        this.detailResponse = detailResponse;
    }
    public void setContext(Context context) {
        this.myFuckingContext = context;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_event_detail, parent, false);
        return new EventDetailHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventDetailHolder eventDetailHolder = (EventDetailHolder)holder;
        eventDetailHolder.event_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myFuckingContext, EventInfoActivity.class);
                myFuckingContext.startActivity(intent);
            }
        });
        int title_length = detailResponse.getActivities().get(position).getTitle().length();
        String title = detailResponse.getActivities().get(position).getTitle();
        if(title_length > 18) {
            title = title.substring(0,18) + "...";
            eventDetailHolder.tv_event_name.setText(title);
        }
        else {
            eventDetailHolder.tv_event_name.setText(title);
        }


    }

    @Override
    public int getItemCount() {
        return detailResponse.getActivities().size();
        //return 5;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
