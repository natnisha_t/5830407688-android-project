package com.example.natnisha.eventy.Fragment_bottom_bar;


import android.content.Context;
import android.os.AsyncTask;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.Button;

import com.example.natnisha.eventy.DOM.DetailResponse;
import com.example.natnisha.eventy.MainActivity;
import com.example.natnisha.eventy.EventInfoActivity;
import com.example.natnisha.eventy.R;
import com.example.natnisha.eventy.holder.EventDetailHolder;
import com.example.natnisha.eventy.recycle_view.EventListAdapter;
import com.example.natnisha.eventy.retrofit.RetrofitInterface;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import static android.content.ContentValues.TAG;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment{

    private RecyclerView recyclerView;
    private EventListAdapter eventListAdapter;
    private View myView;
    private static Context myContext;

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.fragment_home, container, false);
        setUpRecyclerView();
        myContext = this.getActivity();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.kku.ac.th/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        retrofitInterface.getEventList().enqueue(new retrofit2.Callback<DetailResponse>() {
            @Override
            public void onResponse(Call<DetailResponse> call, retrofit2.Response<DetailResponse> response) {
                eventListAdapter = new EventListAdapter();
                eventListAdapter.setContext(myContext);
                eventListAdapter.setData(response.body());
                recyclerView.setAdapter(eventListAdapter);
                eventListAdapter.notifyDataSetChanged();
                Log.d("test", "test");
            }

            @Override
            public void onFailure(Call<DetailResponse> call, Throwable t) {

            }
        });
//        try {
//            String temp = new FeedJSONTask().execute().get();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
        return myView;
    }

//    public class FeedJSONTask extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... params) {
//            feedJSON();
//            return null;
//        }
//        @Override
//        protected  void onPostExecute(String s) {
//            super.onPostExecute(s);
//        }
//    }
//
//    private void feedJSON() {
//        try {
//            OkHttpClient client = new OkHttpClient();
//            final Gson gson = new Gson();
//            Request request = new Request.Builder()
//                    .url("https://www.kku.ac.th/ikku/api/activities/services/topActivity.php")
//                    .build();
//
//            client.newCall(request).enqueue(new Callback() {
//                @Override
//                public void onFailure(Request request, IOException e) {
//                    e.printStackTrace();
//                }
//
//                @Override
//                public void onResponse(Response response) throws IOException {
//                    DetailResponse detailResponse = gson.fromJson(response.body().string(), DetailResponse.class);
//                    EventListAdapter.detailResponse = detailResponse;
//                    Log.d("test", detailResponse.getActivities().get(0).getTitle());
//
//                }
//            });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void setUpRecyclerView() {
        Log.d("test", "1234");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView = (RecyclerView) myView.findViewById(R.id.rv_event_list);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(20);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }

    public HomeFragment() {

    }

}
