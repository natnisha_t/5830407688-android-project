package com.example.natnisha.eventy.recycle_view;

import com.example.natnisha.eventy.recycle_view.BaseOrderDetailItem;

/**
 * Created by natnisha on 22/2/2018 AD.
 */

public class TitleItem extends BaseOrderDetailItem {
    private String title;

    public TitleItem(int type) {
        super(type);
    }
}
