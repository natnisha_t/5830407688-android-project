package com.example.natnisha.eventy.recycle_view;

/**
 * Created by natnisha on 22/2/2018 AD.
 */

public class BaseOrderDetailItem {
    private int type;

    public BaseOrderDetailItem(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
